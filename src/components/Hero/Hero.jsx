import React from 'react';

import './Hero.scss';

export default function Hero(props) {
  const { heading, description, caption, image, alt } = props;

  return (
    <section id='demo' className='hero-section-container'>
      <article className='hero-section-article'>
        <h1 className='heading'>{heading}</h1>
        <p className='description'>{description}</p>
        <p className='caption'>{caption}</p>
        <a href='/' className='view-demo-button custom-button'>
          View Demo
        </a>
      </article>
      <picture className='hero-banner-image-wrapper'>
        <img className='hero-banner-image' src={image} alt={alt} />
      </picture>
    </section>
  );
}
