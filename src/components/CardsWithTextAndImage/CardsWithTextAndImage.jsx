import React from 'react';

import { CardsWithImage } from '../../components';
import './CardsWithTextAndImage.scss';

const WidgetTabs = () => (
  <nav className='navbar'>
    <ul className='nav-items'>
      <li className='nav-item-container '>
        <a href='/' className='nav-item active'>
          UI Components
        </a>
      </li>
      <li className='nav-item-container'>
        <a href='/' className='nav-item'>
          React-Vis Visuals
        </a>
      </li>
      <li className='nav-item-container'>
        <a href='/' className='nav-item'>
          D3 Visuals
        </a>
      </li>
    </ul>
  </nav>
);

export default function CardsWithTextAndImage(props) {
  const {
    id,
    sectionContainerClass,
    cardsContainerClass,
    isLoveK2,
    isTechStack,
    isAvailAbleWidgets,
    heading,
    loveK2Data,
    techStacks,
    widgets,
    imageWrapperClass,
  } = props;
  return (
    <>
      <section id={id} className={sectionContainerClass}>
        {/* heading */}
        <section className='title'>
          <h1 className='center'>{heading}</h1>
          {isTechStack && (
            <figure className='tech-stack-widgets-card-image-wrapper'>
              <img src='assets/images/techStack/logo.png' alt='k2 logo' />
            </figure>
          )}
        </section>

        {/* tabs in widgets */}
        {isAvailAbleWidgets && <WidgetTabs />}

        {/* cards */}
        <article className={cardsContainerClass}>
          {isLoveK2 && (
            <CardsWithImage
              isLoveK2={true}
              cardsData={loveK2Data}
              imageWrapperClass={imageWrapperClass}
            />
          )}
          {isTechStack && (
            <CardsWithImage
              isTechStack={true}
              cardsData={techStacks}
              imageWrapperClass={imageWrapperClass}
            />
          )}
          {isAvailAbleWidgets && (
            <CardsWithImage
              cardsData={widgets}
              imageWrapperClass={imageWrapperClass}
            />
          )}
        </article>

        {/* paragraph */}
        {isLoveK2 && (
          <p className='description love-k2-description'>
            Take a look at
            <a href='/' className='description love-k2-description-link'>
              Docs
            </a>
            for details and more goodies
          </p>
        )}
      </section>
    </>
  );
}
