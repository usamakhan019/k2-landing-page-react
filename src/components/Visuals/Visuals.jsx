import React from 'react';

import { VisualCard } from '../../components';
import { visualCardData } from '../../mockData';
import './Visuals.scss';

export default function Visuals() {
  return (
    <section className='visuals'>
      <h1 className='heading'>Don't see the visual you need? No problem!</h1>
      <section className='visuals-container'>
        <VisualCard visualCardData={visualCardData} />
      </section>
    </section>
  );
}
