import React from 'react';

export default function CardsWithImage(props) {
  const { cardsData, isLoveK2, imageWrapperClass } = props;

  return cardsData.map((data, index) => (
    <figure
      className={imageWrapperClass}
      key={index}>
      <img className='image' src={data.image} alt={data.alt} />
      <figcaption className={isLoveK2 && 'love-k2-image-caption'}>
        {data.caption}
      </figcaption>
    </figure>
  ));
}
