import React from 'react';

import './Roadmap.scss';

export default function Roadmap(props) {
  const { roadmapNext, roadmapBacklog } = props;

  return (
    <section id='roadmap' className='roadmap'>
      <h1 className='heading'>Roadmap</h1>
      <section className='roadmap-container'>
        <article className='roadmap-article next'>
          <h2 className='sub-heading'>Next in line</h2>
          <ul>
            {roadmapNext.map((data, index) => (
              <li key={index}>{data}</li>
            ))}
          </ul>
        </article>

        <article className='roadmap-article backlog'>
          <h2 className='sub-heading'>In the backlog</h2>
          <ul>
            {roadmapBacklog.map((data, index) => (
              <li key={index}>{data}</li>
            ))}
          </ul>
        </article>
      </section>
    </section>
  );
}
