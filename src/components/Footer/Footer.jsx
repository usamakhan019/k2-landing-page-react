/* eslint-disable react/jsx-no-target-blank */
import React from 'react';

import './Footer.scss';

export default function Footer(props) {
  const { footerItems } = props;

  return (
    <footer className='footer' id='storyBook'>
      <section className='brand-services-container'>
        <article className='brand'>
          <figure>
            <img
              className='k2-logo'
              src='assets/images/logo/logo.png'
              alt='dad'
            />
            <figcaption className='product-image'>
              A Product by
              <a href='/'>
                <img src='assets/images/logo/logo2.svg' alt='product figure' />
              </a>
            </figcaption>
            <p className='location'>Islamabad | Santa Clara</p>
          </figure>
        </article>
        <article className='services'>
          {footerItems.map((data, index) => (
            <a target='_blank' className='service' href={data.url} key={index}>
              {data.title}
            </a>
          ))}
        </article>
      </section>
      <p className='paragraph-center center'>
        Copyright © 2020 Emumba Inc. All rights reserved.
      </p>
    </footer>
  );
}
