import React from 'react';

import './Contact.scss';

export default function Contact() {
  return (
    <section className='get-in-touch'>
      <h1 className='heading get-in-touch-heading'>
        Get in touch for any questions
      </h1>
      <section className='contact'>
        <figure className='contact-image-wrapper'>
          <img
            src='assets/images/contact/contact.png'
            alt='contact pic'
            className='contact-image'
          />
        </figure>
        <form className='contact-form'>
          <input
            type='text'
            placeholder='Full Name'
            className='name-input custom-input-field'
          />
          <input
            type='email'
            placeholder='Email Address'
            className='email-input custom-input-field'
          />
          <textarea
            cols='30'
            rows='10'
            placeholder='Message'
            className='message-input custom-input-field'
          />
          <button
            href='/'
            className='customButton get-in-touch-button custom-button'>
            Get in Touch
          </button>
        </form>
      </section>
    </section>
  );
}
