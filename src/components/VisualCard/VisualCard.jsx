import React from 'react';

import './VisualCard.scss';

export default function VisualCard(props) {
  const { visualCardData } = props;
  return (
    <>
      {visualCardData.map((data, index) => (
        <section className={data.sectionClass}>
          <article className={data.articleClass}>
            <h2 className='heading'>{data.heading}</h2>
            <p className='description'>{data.description}</p>
          </article>
          <figure className='absolute-shape'>
            <img src={data.image} alt='' />
          </figure>
        </section>
      ))}
    </>
  );
}
