import React from 'react';

import './ImageWithText.scss';

export default function ImageWithText(props) {
  const { heading, subHeading, description, image, isReverse } = props;

  return (
    <React.Fragment>
      <section id={isReverse ? 'docs' : null}>
        <section className='image-with-text-container'>
          <figure className={isReverse && 'image-with-text-reverse-image'}>
            <img
              src={image}
              alt='rocket and components img'
              className='why-k2-and-help-image'
            />
          </figure>
          <article >
            <h1 className='heading'>{heading} </h1>
            <h2 className='sub-heading'>{subHeading}</h2>
            <p className='description'>{description}</p>
          </article>
        </section>
      </section>
    </React.Fragment>
  );
}
