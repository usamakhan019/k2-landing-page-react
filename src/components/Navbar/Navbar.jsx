/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/jsx-no-target-blank */
import React from 'react';

import Scrollspy from 'react-scrollspy';
import { Link } from 'react-router-dom';

import './Navbar.scss';

export default function Navbar(props) {
  const { navItems } = props;

  return (
    <section className='navbar-container'>
      <nav className='navbar '>
        <a href='/' className='logo'>
          <img src='/assets/images/logo/logo.svg' alt='k2 logo' />
        </a>

        <input id='dropdown' className='input-box' type='checkbox' />

        <label htmlFor='dropdown' className='dropdown'>
          <span className='hamburger'>
            <span className='icon-bar top-bar' />
            <span className='icon-bar middle-bar' />
            <span className='icon-bar bottom-bar' />
          </span>
        </label>
        <Scrollspy
          items={['demo', 'docs', 'techStack', 'roadmap', 'storyBook']}
          currentClassName='active'
          scrollDuration='10'
          className='nav-links-container nav-links'>
          {/* <ul className='nav-links'> */}
          {navItems.map((item, index) => (
            <li key={index} className='nav-item-wrapper'>
              <a
                href={item.url}
                className='nav-item '
                // target={item.isExternalLink && '_blank'}
              >
                {item.title}
              </a>
            </li>
          ))}

          <li className='nav-item-wrapper'>
            <Link to='/privacy' className='nav-item '>
              <a className='nav-item '> Privacy Policy</a>
            </Link>
          </li>
          <li className='nav-item-wrapper'>
            <Link to='/labs' className='nav-item '>
              <a className='nav-item '> Labs</a>
            </Link>
          </li>
          <li className='nav-item-wrapper'>
            <a href='#pricing' className='navbar-buy-now-button custom-button'>
              Buy Now
            </a>
          </li>
          {/* </ul> */}
        </Scrollspy>
      </nav>
    </section>
  );
}
