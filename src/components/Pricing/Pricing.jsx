import React from 'react';

import './Pricing.scss';

export default function Pricing(props) {
  const { pricingFeature } = props;
  return (
    <section id='pricing' className='pricing'>
      <h1 className='heading '>Pricing</h1>
      <section className=' pricing-container'>
        <article className='name pricing-article'>
          <p className='hidden'></p>
          {pricingFeature.map((feature, index) => (
            <p className='pricing-paragraph' key={index}>
              {feature}
            </p>
          ))}
          <p className='hidden'></p>
        </article>

        <Developer />
        <Enterprise />
      </section>
    </section>
  );
}

function Developer() {
  return (
    <article className='developer-licence pricing-article'>
      <p className='heading'>Developer License</p>
      <p className='pricing-paragraph'>$49 per year</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>01</p>
      <p className='pricing-paragraph'>Unlimited</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>15</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>

      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/cross.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <button href='/' className='custom-button pricing-buy-now-button'>
          Buy Now
        </button>
      </p>
    </article>
  );
}

function Enterprise() {
  return (
    <article className='enterprise-licence pricing-article'>
      <p className='heading'>Enterprise License</p>
      <p className='pricing-paragraph'>Custom</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>Unlimited</p>
      <p className='pricing-paragraph'>Unlimited</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>Custom</p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <img src='assets/images/pricing/tick.svg' alt='' />
      </p>
      <p className='pricing-paragraph'>
        <button href='/' className='custom-button pricing-buy-now-button'>
          Buy Now
        </button>
      </p>
    </article>
  );
}
