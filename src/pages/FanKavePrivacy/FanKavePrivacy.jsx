import React from 'react';

import './FanKavePrivacy.scss';

export default function FanKavePrivacy(props) {
  const { PrivacyPolicies } = props;
  return (
    <main className='privacy-policy-container'>
      <section className='max-width-container'>
        {PrivacyPolicies.map((PrivacyPolicy, index) => (
          <article key={index} className='privacy-policy'>
            <h2 className='privacy-policy'>{PrivacyPolicy.title}</h2>
            {PrivacyPolicy.caption && (
              <p className='privacy-policy-caption'>{PrivacyPolicy.caption}</p>
            )}
            <p className='privacy-policy-description'>
              {PrivacyPolicy.description}
            </p>
          </article>
        ))}
        <p className='privacy-policy-responsibility'>
          <strong>
            FANKAVE IS NOT RESPONSIBLE FOR THE PRODUCTS, PRICES, PRIVACY
            PRACTICES OR CONTENT OF OTHER THIRD-PARTY SITES
          </strong>
        </p>
      </section>
    </main>
  );
}
