import React from 'react';

import {
  Hero,
  Visuals,
  Roadmap,
  Pricing,
  Contact,
  ImageWithText,
  CardsWithTextAndImage,
} from '../../components';

import {
  pricingFeature,
  pricing,
  widgets,
  techStacks,
  roadmapNext,
  roadmapBacklog,
  loveK2Data,
} from '../../mockData';

import './LandingPage.scss';

export default function landingPage() {
  return (
    <React.Fragment>
      <main className='max-width-container'>
        <Hero
          heading='K2 is the fastest way to build coolest dashboards'
          description='K2 is a dashboarding framework that lets developers create stunning dashboards at supersonic speed, using React and TypeScript. It comes with rich out-of-the-box features, smart defaults, and modular & extensible design.'
          caption='Visuals from any JavaScript library such as HighCharts, React-Vis, Victory, can be used with K2'
          image='assets/images/hero/k2-banner.png'
          alt='k2-banner dashboard image'
        />
        <ImageWithText
          heading='Why K2?'
          subHeading='K2 makes it easier to have a fancier dashboard'
          description='Building a dashboard requires handling layouts, coloring & theming, fetching and manipulating data, pixel perfection, testing, ... and the list goes on. K2 makes your life easier by handling all these complexities for you. Using K2 you can build beautiful dashboards in a matter of minutes and hours. K2 helps your business grow faster by saving your time and money.'
          image='assets/images/whyK2/rocket.png'
        />
        {/* <LoveK2 loveK2Data={loveK2Data} /> */}
        <CardsWithTextAndImage
          heading='Developers love K2 the most because'
          loveK2Data={loveK2Data}
          isLoveK2={true}
          cardsContainerClass='love-k2-cards-container'
          sectionContainerClass='love-k2'
          imageWrapperClass='love-k2-image-wrapper'
        />
        <CardsWithTextAndImage
          id='techStack'
          heading='Technology Stack of'
          techStacks={techStacks}
          isTechStack={true}
          cardsContainerClass='tech-stack-widgets-cards-wrapper cards-section-top-space'
          sectionContainerClass='tech-stack-widgets-container '
          imageWrapperClass='tech-stack-widgets-card-image-wrapper'
        />
        {/* <TechStack techStacks={techStacks} /> */}
        <ImageWithText
          heading='Help & Support'
          subHeading='We take care of our customers'
          description='While using K2, if you have any questions, we will pair you with a member of the team who built K2. We have a large community that is ever-ready to help you succeed.'
          image='assets/images/help/help.png'
          isReverse={true}
          techStacks={techStacks}
        />
        {/* <Widgets widgets={widgets} /> */}
        <CardsWithTextAndImage
          heading='Available Widgets'
          widgets={widgets}
          isAvailAbleWidgets={true}
          cardsContainerClass='tech-stack-widgets-cards-wrapper'
          sectionContainerClass='tech-stack-widgets-container'
          imageWrapperClass='tech-stack-widgets-card-image-wrapper'
        />
        <Visuals />
        <Roadmap roadmapNext={roadmapNext} roadmapBacklog={roadmapBacklog} />
        <Pricing pricingFeature={pricingFeature} pricing={pricing} />
        <Contact />
      </main>
    </React.Fragment>
  );
}
