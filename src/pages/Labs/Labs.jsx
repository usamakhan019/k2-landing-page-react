import React from 'react';

import './Labs.scss';

const Header = () => (
  <header className='labs-header'>
    <h1 className='center'>Labs</h1>
    <p className='labs-description'>
      It is imperative to stay at the cutting edge of innovation. The purpose of
      these labs is to encourage disruption and experimentation in various
      technologies listed below and more. Ideas born in our labs eventually
      mature into our core service or solution.
    </p>
  </header>
);

const CardsContainer = ({LabsCardsData}) => (
  <section className='labs-cards-container'>
    {LabsCardsData.map((cardData, index) => (
      <article key={index} className='labs-card'>
        <figure
          className={
            cardData.isYellowImage
              ? 'labs-card-image yellow-image'
              : 'labs-card-image blue-image'
          }>
          {cardData.image}
        </figure>
        <figcaption className='labs-card-captions'>
          <h2>{cardData.title}</h2>
          <p>{cardData.caption}</p>
        </figcaption>
      </article>
    ))}
  </section>
);
export default function Labs(props) {
  const { LabsCardsData } = props;
  return (
    <section className='labs-container'>
      <Header />
      <CardsContainer LabsCardsData={LabsCardsData} />
    </section>
  );
}
