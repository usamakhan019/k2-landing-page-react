import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { LandingPage, FanKavePrivacy, Labs } from './pages';
import { Navbar, Footer } from './components';
import {
  navItems,
  footerItems,
  PrivacyPolicies,
  LabsCardsData,
} from './mockData';
import './App.scss';

function App() {
  return (
    <BrowserRouter>
      <Navbar navItems={navItems} />
      <Switch>
        <Route exact path='/' component={LandingPage}/>
        <Route
          path='/privacy'
          render={() => (
            <FanKavePrivacy PrivacyPolicies={PrivacyPolicies} />
          )}/>
        <Route
          path='/labs'
          render={() => <Labs LabsCardsData={LabsCardsData} />}/>
      </Switch>
      <Footer footerItems={footerItems} />
    </BrowserRouter>
  );
}

export default App;
