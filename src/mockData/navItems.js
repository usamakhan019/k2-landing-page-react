export const navItems = [
  { title: 'Demo', url: '#demo', isExternalLink: true },
  {
    title: 'Docs',
    url: '#docs',
    isExternalLink: true,
  },
  { title: 'Tech Stack', url: '#techStack' },
  { title: 'Roadmap', url: '#roadmap' },
  {
    title: 'StoryBook',
    url: '#storyBook',
    isExternalLink: true,
  },
];
