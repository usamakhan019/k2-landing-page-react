export const visualCardData = [
  {
    sectionClass: 'visual-card integrate',
    articleClass: 'visuals-card-container visuals-integrate-article',
    heading: 'Request a custom visualization',
    description:
      'Want to use a visual from a charting library like Highcharts, React Vis, Victory, etc? No problem, K2 provides seamless integration with any modern JS charting library!',
    image: 'assets/images/visuals/Shape1.png',
  },
  {
    sectionClass: 'visual-card request',
    articleClass: 'visuals-card-container visuals-request-article',
    heading: 'Integrate a custom visualization',
    description:
      'Get in touch with us with your business needs and we will cook up a custom visualization tailored for you.',
    image: 'assets/images/visuals/Shape2.png',
  },
];
