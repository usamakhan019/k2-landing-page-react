import React from 'react';
import { TickIcon } from '../assets';

export const pricingFeature = [
  'License fee',
  'Fixes and updates',
  'No of developers',
  'No of applications',
  'Access to 1st line support',
  'Support hours per license',
  'Access to 2nd line support by core developers',
  'Chat support',
  'Guidance on implementing K2 with 3rd party systems',
  'Use K2 in a software through which your customers can generate additional applications',
];

export const pricing = [
  {
    title: '',
    developerLicense: 'Developer License',
    enterpriseLicense: 'Enterprise License',
  },
  {
    title: 'License fee',
    developerLicense: '$49 per year',
    enterpriseLicense: 'Custom',
  },
  {
    title: 'Fixes and updates',

    // developerImage: 'assets/images/pricing/tick.svg',
    // enterpriseImage: 'assets/images/pricing/tick.svg',
    developerLicense: <TickIcon />,
    enterpriseLicense: <TickIcon />,
  },
  {
    title: 'No of developers',
    developerLicense: 'Developer License',
    enterpriseLicense: 'Enterprise License',
  },
  {
    title: '',
    developerLicense: '01',
    enterpriseLicense: 'Unlimited',
  },
  {
    title: 'No of applications',
    developerLicense: 'Unlimited',
    enterpriseLicense: 'Unlimited',
  },
  {
    title: 'Access to 1st line support',

    developerImage: 'assets/images/pricing/tick.svg',
    enterpriseImage: 'assets/images/pricing/tick.svg',
  },
  {
    title: 'Support hours per license',
    developerLicense: '15',
    enterpriseLicense: 'Custom',
  },
  {
    title: 'Access to 2nd line support by core developers',

    developerImage: 'assets/images/pricing/tick.svg',
    enterpriseImage: 'assets/images/pricing/tick.svg',
  },
  {
    title: 'Chat support',

    developerImage: 'assets/images/pricing/tick.svg',
    enterpriseImage: 'assets/images/pricing/tick.svg',
  },
  {
    title: 'Guidance on implementing K2 with 3rd party systems',

    developerImage: 'assets/images/pricing/tick.svg',
    enterpriseImage: 'assets/images/pricing/tick.svg',
  },
  {
    title:
      'Use K2 in a software through which your customers can generate additional applications',

    developerImage: 'assets/images/pricing/cross.svg',
    enterpriseImage: 'assets/images/pricing/tick.svg',
  },
];
