export const roadmapNext = [
  'Server-side rendering support',
  'Allow periodic data updates via single prop/flag',
  'Accessibility improvements',
  'More tutorials and documentation improvements',
  'New components to make grids and display logs',
];


export const roadmapBacklog = [
  'Let each user personalize the layout on the fly',
  'Prebuilt dashboard templates for common use cases',
  'More custom visualizations',
  " Cognitive dashboards that take viewer's attention directly to what is important",
  'New components to make grids and display logs',
];