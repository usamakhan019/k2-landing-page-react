export const PrivacyPolicies = [
  {
    title: 'Privacy Policy',
    description:
      'This privacy policy sets out how FanKave, Inc. (“Company” or ‘We”) uses and protects any information that we may collect from you or that you may provide when you use the website “fankave.com” (our “Site”). By accessing this Site and our mobile app(s), you agree that you have read and understood the Company’s privacy statement, which describes how we use your personal information, and what options you have to update or request the deletion or correction of information submitted to us via the Site. If you do not agree with the terms and conditions of this Statement, please do not use the Site or mobile app.',
  },
  {
    title: 'INFORMATION COLLECTED',
    description:
      'Personal Information Submitted By You. We collect Personal Information you provide to us for purposes of (a) signing up for our email service (b) submitting comments to us regarding vendors, products for this Site (c) posting listings on our site (d) self-publishing and confirmation emails, (e) authenticating user accounts (f) providing subscription email services (g) registering, etc. All postings are stored in our database and may be archived elsewhere. The practices described in this Statement may also apply to Information you provide to us in writing or by telephone, such as when you contact our customer service staff. Personal Information may include items such as your name, address, telephone number, zip code and age. Unless you provide it to us voluntarily, We do not collect Personal Information about you in connection with your use of this Site or through our mobile apps.',
  },
  {
    title: 'COOKIES',
    description:
      'Cookies are small data files that are stored on an Internet user&#39;s computer by a web server and may be used by some of the advertisers in the ads contained in our Site.',
  },
  {
    title: 'INFORMATION PERTAINING TO MINORS',
    description:
      'We do not wish to collect Personal Information from minors (children under 18 years of age, or any other age defined under applicable law). If we become aware that a minor is attempting to or has submitted Personal Information via this Site, we will notify the user that we may not accept his or her Personal Information. We will then delete any such Personal Information from our records.',
  },
  {
    title: 'TRANSACTIONAL INFORMATION',
    description:
      'Certain information may be collected automatically as part of your use of this site or our mobile apps, as well as from your transactions with us and our affiliates or non-affiliated third parties. This Non-personal Information is used to improve the overall Site operations, functionality and appearance. Non-personal Information may include your web browser type, domain name, date/time and IP address, upon which you visited the Site.',
  },
  {
    title: 'USAGE',
    description:
      'Personal Information is used by the Company for the purpose for which it was submitted. For example, if you submit your email address and other information required to subscribe to our newsletter, we will use that Personal Information to send you our newsletter. We use your IP address to help diagnose problems with our server, and to administer the Site.',
  },
  {
    title: 'THIRD PARTIES & LINKS',
    description:
      'Linking: This Statement applies only to https://fankave.com. This Site or our mobile apps may include links to other third party web sites including access to content, products and services of such affiliated and non-affiliated entities. Whenever you choose to access third party content via this site or mobile app, you maybe taken to the third-parties website. We urge you to familiarize yourself with the individual privacy and other terms for each linked site prior to submitting your Personal Information.',
  },
  {
    title: 'DISCLOSURE OF INFORMATION TO THIRD PARTIES',
    caption:
      'Demographic and aggregate Information may be shared by us with third party advertisers and retail partners.',
    description:
      'We make every reasonable effort to preserve user privacy. However we reserve the right to disclose Personal Information when required or permitted by law and we have a good-faith belief that such action is necessary to comply with an appropriate law enforcement investigation, current judicial proceeding, a court order or legal process served on us.',
  },
  {
    title: 'SECURITY',
    description:
      'This Site has security measures in place to protect the loss, misuse and alteration of the Information under our control. All employees are aware of our policies. Safeguards are in place to keep sensitive data secure via the use of passwords.',
  },
  {
    title: 'SALE OR TRANSFER OF INFORMATION',
    description:
      'In the event of a sale, merger, liquidation, dissolution, or transfer of part of the business, trade or assets of the Company, all Information collected about you via this Site may be sold, assigned, or transferred to the party acquiring all or substantially all of the equity or assets or business of the Company. Subject to your election to opt out as described below, by using this Site, you consent to the sale and transfer of your information as described in this paragraph.',
  },
  {
    title: 'OTHER TERMS',
    description:
      'Your use of our Site or our mobile apps is subject to and contingent upon your agreement with our Terms of Use, which are hereby incorporated by reference.',
  },
  {
    title: 'NOTIFICATION OF CHANGES',
    description:
      'We reserve the right to modify or change any privacy policies at anytime without any notice to you. Any modifications will be effective immediately as of the date the modified Privacy Policy is posted on our website or through our mobile apps.',
  },
  {
    title: 'OPT-OUT FROM NEWSLETTERS',
    description:
      'Our Site provides users the opportunity to opt-out of receiving communications from us and our partners when you subscribe to our newsletter. You can always opt-out from receiving emails from us.',
  },
  {
    title: 'CONSENT',
    description:
      'By using this Site, you consent to the use, storage and disclosure of your Information by us in the manner described in this Statement. We reserve the right to make changes to this Statement from time to time and will alert you to such changes via the Notification procedure described in the preceding paragraph.',
  },
  {
    title: 'QUESTIONS REGARDING STATEMENT',
    description:
      'If you have questions or concerns regarding this Statement, you should first contact the Webmaster by email at support@fankave.com',
  },
];
