export const loveK2Data = [
  {
    image: 'assets/images/loveK2/typescript.svg',
    alt: 'typescript img',
    caption: 'Built using React and TypeScript',
  },
  {
    image: 'assets/images/loveK2/extensive-library.svg',
    alt: 'extensive-library',
    caption: 'Extensive library of visuals',
  },
  {
    image: 'assets/images/loveK2/custom-visualization.svg',
    alt: 'custom-visualization',
    caption: 'Custom visualizations on demand',
  },
  {
    image: 'assets/images/loveK2/theme-support.svg',
    alt: 'theme-support',
    caption: ' First class theming support',
  },
  {
    image: 'assets/images/loveK2/dark-mode.svg',
    alt: 'dark-mode',
    caption: 'Dark Mode',
  },
  {
    image: 'assets/images/loveK2/drill-down.svg',
    alt: 'drill-down',
    caption: 'Supports drill downs into visuals',
  },
  {
    image: 'assets/images/loveK2/js.svg',
    alt: 'js',
    caption:
      'Integratable with any JS library including Highcharts,React-vis victory',
  },
  {
    image: 'assets/images/loveK2/react.svg',
    alt: 'react',
    caption: 'Seamless integration with Create React App',
  },
  {
    image: 'assets/images/loveK2/easy-integration.svg',
    alt: 'easy-integration',
    caption: 'Super easy integration with REST APIs',
  },
  {
    image: 'assets/images/loveK2/tree-shaking.svg',
    alt: 'tree-shaking',
    caption: 'Tree Shaking',
  },
  {
    image: 'assets/images/loveK2/data-filters.svg',
    alt: 'data-filters',
    caption: 'Custom data filters per visual',
  },
  {
    image: 'assets/images/loveK2/documentation.svg',
    alt: 'documentation',
    caption: 'Extensive documentation and code examples',
  },
];

export const techStacks = [
  {
    image: 'assets/images/techStack/Styled-components.png',
    alt: 'Styled-components',
    caption: 'Styled-components',
  },
  {
    image: 'assets/images/techStack/React.png',
    alt: 'React',
    caption: 'React',
  },
  {
    image: 'assets/images/techStack/d3.png',
    alt: 'D3',
    caption: 'D3',
  },
  {
    image: 'assets/images/techStack/TypeScript.png',
    alt: 'TypeScript',
    caption: 'TypeScript',
  },
  {
    image: 'assets/images/techStack/Jest.png',
    alt: 'Jest',
    caption: 'Jest',
  },
  {
    image: 'assets/images/techStack/goat.png',
    alt: 'React-Testing- Library',
    caption: 'React-Testing- Library',
  },
  {
    image: 'assets/images/techStack/Vis.png',
    alt: 'React-Vis',
    caption: 'React-Vis',
  },
];

export const widgets = [
  {
    image: 'assets/images/widgets/alert.png',
    caption: 'Alert',
    alt: 'Alert',
  },
  {
    image: 'assets/images/widgets/Card.png',
    caption: 'Card',
    alt: 'Card',
  },
  {
    image: 'assets/images/widgets/table.png',
    caption: 'Data Table',
    alt: 'Data Table',
  },
  {
    image: 'assets/images/widgets/legends.png',
    caption: 'legends',
    alt: 'legends',
  },
  {
    image: 'assets/images/widgets/Theme.png',
    caption: 'Toggle Theme',
    alt: 'Toggle Theme',
  },
  {
    image: 'assets/images/widgets/V2.png',
    caption: 'V2 Tile',
    alt: 'V2 Tile',
  },
  {
    image: 'assets/images/widgets/Grid.png',
    caption: 'Grid Layout',
    alt: 'Grid Layout',
  },
];
