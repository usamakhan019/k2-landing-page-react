import React from 'react';

import { BlockChain, Embedded } from '../assets';

export const LabsCardsData = [
  {
    image: <Embedded />,
    title: 'Embedded Systems',
    caption: 'Embedded Systems - Let’s Partner for the Connected World',
    isYellowImage:true
  },
  {
    image: <BlockChain />,
    title: 'Blockchain',
    caption: 'Decentralized, Transparent and Immutable Digital Ledger',
  },
];
