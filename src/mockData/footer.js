export const footerItems = [
  {
    title: 'License',
    url: 'https://k2.emumba.com/files/K2%20Developer%20License.Agreement.pdf',
  },
  { title: 'Demo', url: 'https://demos.emumba.com/' },
  { title: 'Docs', url: 'https://k2.emumba.com/docs/introduction/' },
  { title: 'StoryBook', url: 'https://k2.emumba.com/storybook/?path=/story/*' },
  { title: 'Emumba', url: 'https://emumba.com/' },
  { title: 'Contact us', url: 'mailto:k2@emumba.com?subject=K2 - Contact Us' },
];
